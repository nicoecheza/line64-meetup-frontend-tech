import Comment from './Comment';

export default class Comments extends React.Component {
  constructor(props) {
    super(props);
  }
  renderComments() {
    let comments  = this.props.comments = [1,2,3,4];
    return comments.map((comment) => <Comment 
                                    author={ comment.author }  
                                    comment={ comment.comment }
                                    YES_HE_CAN_EDIT={ comment.canEdit? }
                                  />);
  }
  render() {

    return (

      { this.renderComments() }
      
    );

  }
}